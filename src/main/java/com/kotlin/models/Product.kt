package com.kotlin.models

import com.kotlin.enums.MeasurementUnit
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("application.product")
data class Product (
    @Id val id: Long? = null,
    val name: String,
    val description: String,
    val warehouseId: Long,
    val measurementUnit: MeasurementUnit
        )