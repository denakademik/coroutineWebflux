package com.kotlin.models

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("application.warehouse")
data class Warehouse(
    @Id val id: Long? = null,
    val name: String,
    val address: String,
    val description: String
        )