package com.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class CoroutinesWebFluxApplication

    fun main(args: Array<String>) {
        runApplication<CoroutinesWebFluxApplication>(*args)
    }
