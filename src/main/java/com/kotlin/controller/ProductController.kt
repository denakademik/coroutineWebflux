package com.kotlin.controller

import com.kotlin.dto.ProductRequest
import com.kotlin.dto.ProductResponse
import com.kotlin.enums.MeasurementUnit
import com.kotlin.models.Product
import com.kotlin.models.Warehouse
import com.kotlin.service.ProductService
import com.kotlin.service.WarehouseService
import kotlinx.coroutines.flow.Flow
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import kotlin.random.Random

@RestController
@RequestMapping("/api/products")
class ProductController(private val productService: ProductService, private val warehouseService: WarehouseService){

    @GetMapping
    suspend fun getAllProducts(): Flow<Product>{
        return productService.getAllProducts()
    }

    @GetMapping("/{id}")
    suspend fun getProductById(@PathVariable id:Long): ProductResponse? {
        val product = productService.getProductById(id)

        val warehouse = product?.let { warehouseService.getWarehouseById(it.warehouseId) }
        return warehouse?.let { product.toResponse(it) }
    }



    @PostMapping
    suspend fun saveProduct(@RequestBody product: Product): Product =

        productService.saveProduct(product)


//
//    @GetMapping("/{name}")
//    suspend fun getProductByNameLike(@PathVariable name:String): Flow<Product>{
//        return productService.findAllProductByNameLike(name)
//    }

}

//private fun ProductRequest.toModel(): Product =
//    Product(
//        id = 1,
//        name = this.name,
//        description = this.description,
//        warehouseId = this.warehouseId
//    )
//
private fun Product.toResponse(warehouse: Warehouse): ProductResponse =
    ProductResponse(
        name = this.name,
        description = this.description,
        warehouseName = warehouse.name
    )