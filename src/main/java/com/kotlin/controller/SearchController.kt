package com.kotlin.controller

import com.kotlin.dto.ResultType
import com.kotlin.dto.TypeResponse
import com.kotlin.models.Product
import com.kotlin.models.Warehouse
import com.kotlin.service.ProductService
import com.kotlin.service.WarehouseService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/search")
class SearchController (
    private val productService: ProductService,
    private val warehouseService: WarehouseService
    ){

    @GetMapping
    suspend fun searchByNames(
        @RequestParam(name = "query") query: String
    ): Flow<TypeResponse> {
        val products = productService.findAllProductByNameLike(name = query)
            .map(Product::toTypeResponse)

        val warehouses = warehouseService.findAllWareHousesByNameLike(name = query)
            .map(Warehouse::toTypeResponse)

        return merge(products, warehouses)
    }
}

private fun Product.toTypeResponse():TypeResponse =
    TypeResponse(
        id = this.id!!,
        name  = this.name,
        type = ResultType.PRODUCT
    )

private fun Warehouse.toTypeResponse():TypeResponse =
    TypeResponse(
        id = this.id!!,
        name  = this.name,
        type = ResultType.WAREHOUSE
    )