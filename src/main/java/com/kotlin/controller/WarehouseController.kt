package com.kotlin.controller

import com.kotlin.dto.ProductRequest
import com.kotlin.dto.ProductResponse
import com.kotlin.dto.ResponseWarehouse
import com.kotlin.models.Product
import com.kotlin.models.Warehouse
import com.kotlin.repository.ProductRepository
import com.kotlin.repository.WarehouseRepository
import com.kotlin.service.ProductService
import com.kotlin.service.WarehouseService
import kotlinx.coroutines.flow.toList
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException.NotFound
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("api/warehouses")
class WarehouseController(private val warehouseService: WarehouseService,private val productService: ProductService) {

    @GetMapping("/{id}")
    suspend fun getWarehousesById(@PathVariable id:Long) :ResponseWarehouse =
        warehouseService.getWarehouseById(id)
            ?.let { warehouse ->
                warehouse.toResponse(
                    products = findWarehouseProducts(id)
                )
            }
            ?: throw Throwable()

    @PostMapping
    suspend fun saveWarehouse(@RequestBody warehouse: Warehouse): Warehouse =
        warehouseService.saveWarehouse(warehouse)


    private suspend fun findWarehouseProducts (id: Long) =
        productService.findAllProductsByWarehouseId(id).toList()

}

private fun Warehouse.toResponse(products: List<Product> = emptyList()) : ResponseWarehouse =
    ResponseWarehouse(
        id = this.id!!,
        name = this.name,
        description = this.description,
        products = products
    )


