package com.kotlin.repository

import com.kotlin.models.Warehouse
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WarehouseRepository: CoroutineCrudRepository<Warehouse, Long> {

    fun findByNameContaining(name: String): Flow<Warehouse>
}