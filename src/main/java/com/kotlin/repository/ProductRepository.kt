package com.kotlin.repository

import com.kotlin.models.Product
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository: CoroutineCrudRepository<Product,Long>  {

    fun findByWarehouseId(id:Long): Flow<Product>

    fun findByNameContaining(name:String): Flow<Product>
}