package com.kotlin.dto

import com.fasterxml.jackson.databind.BeanDescription

data class ProductRequest(
    val name: String,
    val description: String,
    val warehouseId: Long
)