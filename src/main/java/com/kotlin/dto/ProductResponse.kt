package com.kotlin.dto

data class ProductResponse(
    val name: String,
    val description: String,
    val warehouseName: String
)