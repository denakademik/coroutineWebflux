package com.kotlin.dto

import com.kotlin.models.Product

data class ResponseWarehouse (
    val id: Long,
    val name: String,
    val description: String,
    val products: List<Product>
        )