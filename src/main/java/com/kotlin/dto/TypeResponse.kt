package com.kotlin.dto

data class TypeResponse(
    val id: Long,
    val name: String,
    val type: ResultType
)

enum class ResultType{
    PRODUCT, WAREHOUSE
}