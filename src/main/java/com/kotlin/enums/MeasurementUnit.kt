package com.kotlin.enums

enum class MeasurementUnit {
    КГ,
    ЛИТР,
    ШТ,
    УПАКОВКА
}
