package com.kotlin.service

import com.kotlin.models.Product
import com.kotlin.models.Warehouse
import com.kotlin.repository.ProductRepository
import kotlinx.coroutines.flow.Flow
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    suspend fun saveProduct(product: Product): Product =
        productRepository.save(product)

    suspend fun getAllProducts(): Flow<Product> =
        productRepository.findAll();

    suspend fun findAllProductByNameLike(name: String): Flow<Product> =
        productRepository.findByNameContaining(name)

    suspend fun findAllProductsByWarehouseId(id:Long): Flow<Product> =
        productRepository.findByWarehouseId(id)

     suspend fun getProductById(id:Long): Product? =
        productRepository.findById(id)

}