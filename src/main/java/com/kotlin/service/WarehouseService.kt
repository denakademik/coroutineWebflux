package com.kotlin.service

import com.kotlin.models.Warehouse
import com.kotlin.repository.WarehouseRepository
import kotlinx.coroutines.flow.Flow
import org.springframework.stereotype.Service

@Service
class WarehouseService(private val warehouseRepository: WarehouseRepository) {

    suspend fun getWarehouseById(id:Long): Warehouse? =
        warehouseRepository.findById(id)

    suspend fun saveWarehouse(warehouse: Warehouse) =
        warehouseRepository.save(warehouse)

    suspend fun findAllWareHousesByNameLike(name: String): Flow<Warehouse> =
        warehouseRepository.findByNameContaining(name)


}